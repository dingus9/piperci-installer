# PiperCI Development Environment

## Installation Methods

### Vagrant

This will provision a virtual machine using Vagrant and install PiperCI.

You will need to download and install PiperCI PiCli manually. Please read the documentation located [here](https://gitlab.com/dreamer-labs/piperci-picli.git)

If you need assistance installing Vagrant please refer to this task file: https://github.com/codrcodz/laptop/blob/master/roles/laptop/tasks/vagrant.yml

Additionally you can refer to the documentation located here: https://www.vagrantup.com/docs/installation/

#### Requirements

```

  * Software:
    `Vagrant`
    `Virtualbox`
    Ansible 2.8
  * `Disk: 32GB`
  * `CPU: 2 vCPU`
  * `RAM: 2GB`

```

#### Installation

##### Debian-based

```
wget https://releases.hashicorp.com/vagrant/2.2.5/vagrant_2.2.5_linux_amd64.zip
unzip vagrant_2.2.5_linux_amd64.zip
apt-get install python3-virtualenv
virtualenv -p python3 ansible
source ansible/bin/activate
pip install ansible
./vagrant up
```

After provisioning is done you can SSH into the Vagrant VM using `vagrant ssh`.

### Docker

This Docker installation method requires Docker version 19.03 or greater.
This will create a Docker Stack on your machine with all the required components utilizing Ansible.

#### Requirements

```
  * Software:
    `Ansible >= 2.8`
    `Docker >= 19.03`
    `Docker (python) >=3.7.3`
  * `Disk: 25GB`
  * `CPU: 2vCPU`
  * `RAM: 2GB`
  * Also requires the ability to create privileged docker containers.
```

#### Installation

```
apt-get install python3-virtualenv
virtualenv -p python3 docker
source docker/bin/activate
pip install docker ansible
ansible-playbook ansible-dind.yml
```

## Using the development environments

Both developer environments have all the required ports forwarded through to the PiperCI infrastructure.

To run a test of the environment one can clone the [PiperCI demo repository](https://gitlab.com/dreamer-labs/piperci/piperci-demo.git)
and run the tests using the PiCli docker container, or by installing PiCli locally.

```
git clone https://gitlab.com/dreamer-labs/piperci/piperci-demo.git
## If using the Vagrant installation, modify the IP addresses in piperci-demo/piperci.d/default/config.yml
## to be '192.168.1.33', or whatever IP address your Vagrant machine has.
docker run -v "${PWD}/piperci-demo":/app registry.gitlab.com/dreamer-labs/piperci/piperci-picli:latest
```
