# 1.0.0 (2019-09-16)


### Features

* add dev env installations ([c0f8bfb](https://gitlab.com/dreamer-labs/piperci/piperci-installer/commit/c0f8bfb))
